|Nama: Amir Hamzah Bin Nor Azmi|Matric.No:198380|
|------|------|

|Logbook Entry: 01| Date: 5/11/2021|
|------|------|

|1. Agenda|
|------------|
|a) Get details on both the design requirements of thruster arms and gondola|
|b) Come out with initial ideas and designs for thruster arms.|

|2. Goals|
|------------|
|a) Brainstorm ideas|
|b) takign measurement from the thruster arm model.|
|c) Draw the current thruster arm model in CAD.|


|3. Decision to solve the problem|
|-------|
|a) Took a visit to Lab H 2.1 to look at the currentthruster arm model.|

|4. Method to solve the problem|
|-----------|
|a) Use measurement tape to take the current model dimensions.|
|b) temporarily sketch the current thruster arm model along with its dimension in a piece of paper.|

|5. Justification when solving the problem|
|------------|
|a) Using measurement tape as it is the most available tool to get a quite accurate measurement|
|b) Sketching on paper first and draw in CAD later at home to save time in the lab|


|6. Impact of/after the decision chosen|
|-------------------|
|a) Time spend in the lab is reduced and been used efficiently.|
|b) Able to draw the exact current thruster arm model with its dimension.|


|7. Our next step|
|----------|
|a) Start integrating some of the ideas to improve the thruster arm model.|
|b) Acquire data to simulate load/force that acts on the thruster arm to test its strength and stability.|
