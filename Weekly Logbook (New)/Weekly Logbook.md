<div align="center">

<img src="https://gitlab.com/viknes10/idp-weekly-logbook/-/raw/main/Upm_Logo.png" width=350 align=middle>

</div>

<div align="center">

### :rocket: Department of Aerospace Engineering :rocket:
### EAS4947-1: PROJEK REKA BENTUK AEROANGKASA 
### (AEROSPACE DESIGN PROJECT)

</div>


## Front cover 

| Heading | Information |
| ------ | ----------- |
| Name | Amir Hamzah Bin Nor Azmi |
| Matric Number   | 198380 |
| Year of Study  | Year 4 |
| Subsystem Group | IDP Design and Structure |
| Team   | 1 |
| Lecturer / Supervisor   | DR. AHMAD SALAHUDDIN BIN MOHD HARITHUDDIN |

## Content of the Log

| Log Book Content | 
| ------ | 
| 1. Agenda   |
| 2. Goals   |
| 3. Problems   |
| 4. Decisions taken to solve problems   |
| 5. Method to solve problems   |
| 6. Justification   |
| 7. Impact of the decision taken into   |
| 8. Next progress or step   |



<div align="center">

## **Page 1**

</div>

<div align="center">

<img src="https://gitlab.com/viknes10/idp-logbook-1/-/raw/main/R__1_.png" width=200 align=middle>

</div>

## Log Book 01 

| No. | Content | Descriptions |
| ------ | ------ | ------ |
| 1 | Agenda|Introductory class for IDP project 2021/2022|
| 2 | Goals |Get to know the project overviews and groups|
| 3 | Problem |No proper way of dividing task between group members|
| 4 | Decision |Divide the group into 2 subgroup|
| 5 | Method |List out the 2 important projects for design and structure group which is the gondola design and structure arm design. |
| 6| Justifications |Gondola and thruster arm designs are two most impostant design for the airship, therefore they need to be pririoritize more. |
| 7 | Impact |Design and structure group has been devided into two subgroup. One that focuses o Gondola and another focuses on thruster arm. |
| 8| Next Step |Construct a gantt chart for the overall design and structure group. |



<div align="center">

## **Page 2**

</div>

## Log book 02

| No. | Content | Descriptions |
| ------ | ------ | ------ |
| 1 | Agenda|Construct a gantt chart for Design and Structure team for the IDP Project.|
| 2 | Goals |Make a consice and logical gantt chart that contains task suitable with its duration to finish. |
| 3 | Problem |The IDP Project timeline overview is still unclaer to some of the group members. |
| 4 | Decision |Discussion is done to have an synchronise understanding on the general idea of the IDP project and cinstruct the task needed for the gantt chart. |
| 5 | Method |Since, not everyone is at campus, the discussion is done through Google Meet with every members attended in the discussion |
| 6| Justifications |Discussion through online method did not only accesscible for everyone but it enabled some of us to take recordings of the discussion to review back in later days |
| 7 | Impact |Every members of the Design team has a clear understanding of the task listed on the gantt chart.|
| 8| Next Step |Get details on the old design of thruster arm and come out with an improved design ideas. |


<div align="center">

## **Page 3**

</div>


## Log book 03

| No. | Content | Descriptions |
| ------ | ------ | ------ |
| 1 | Agenda|Get details on the old design of thruster arm and come out with an improved design ideas.|
| 2 | Goals | Have a CAD drawing of the old thruster arms and come out with a new design improvements |
| 3 | Problem |Unable to obtain the old CAD. Hence, not base structure on what to improve|
| 4 | Decision |Draw the CAD of old thruster arms and try to come out with improvement from the structure. |
| 5 | Method |Take manual measurement of the thruster arm at the lab, drew the thurster arm in SOLIDWORKS and come out with idea to change the pin connection to a tripod connection between the thruster arm and its mount on the body of the airship.|
| 6| Justifications |The pin connection causes the thruster arm to apply a single concetrated force on the body of the airship during operation. Therefore, it is a good idea to distribute the force by using a tripod connection. |
| 7 | Impact |Need more justification and ideas to design the mount for the tripod connection. The weight addition is also been highly considered.|
| 8| Next Step |Draw the tripod connection and come out with a mounitng design for the connection. |

<div align="center">



</div>


<div align="center">

## **Page 4**

</div>
   

## Log book 04

| No. | Content | Descriptions |
| ------ | ------ | ------ |
| 1 | Agenda|Draw the tripod connection and come out with its mounting design.|
| 2 | Goals |To draw the tripod connection and come out with its mounting design.  |
| 3 | Problem |Most of the desing of the tripod on the internet is too complicated to draw on SOLIDWORKS and it might takes a long time to fabricate the tripod usign 3D printer.|
| 4 | Decision |Simplify one of the design from the internet and sketch the tripod in SOLIDWORKS. |
| 5 | Method |Reduces some of the curvature feautures that is unneseccary to the strength of the structure.|
| 6| Justifications |Some of the curvature feauture on the tripod are redundant and not curcial for the integrity of the structure. the curvature feautures will also takes time to be fabricated.|
| 7 | Impact |A more simple design of tripod connection.|
| 8| Next Step |Discuss with the team and Dr.Salah on how doable the tripod coonection regarding the weight it will add to the thruster arm. |

<div align="center">

## **Page 5**

</div>


## Log book 05

| No. | Content | Descriptions |
| ------ | ------ | ------ |
| 1 | Agenda|Discuss the eligibility of the newly design tripod connection for the thruster arm in term of its weight addition.|
| 2 | Goals |To get everyone's opinions and views on how worth it is to use the new thruster arm even though it add quite a lot of weight.|
| 3 | Problem |The discussion concluded that the tripod arm with a tripod connection have added to much weight to the airship. Hence, the idea design is cancelled.|
| 4 | Decision |Due to the addition of the sprayer as an additional load to the airship, wieght of every structure need to be reduced to copromised the additonal weight and it includs the structure of the structure arm. |
| 5 | Method |Just focus on onptimizing the old design of the thruster arm as the old design is already stable enough. The only task is to make it more weight efficient.|
| 6| Justifications |Optimization of the structure is also part of the design team. It is better to focus and finish the task that are possible at the time rather than come out with a new complicated design that might takes extra time to make sure it is optimized enough. |
| 7 | Impact |The tripod connection design has been abandon. The team will now focus on how to optimize the structure by reducing some of its weight.|
| 8| Next Step |Optimize the arm rod and pin connectionof the thruster arm and finalize the desing to the team. |


<div align="center">

</div>

<div align="center">

## **Page 6**

</div>

## Log book 06

| No. | Content | Descriptions | 
| ------ | ------ | ------ |
| 1 | Agenda |Optimize the arm rod and pin connection of the thruster arm and finalize the desing to the team. |
| 2 | Goals |Finish the optimized design of rod and its connecters on the bofy of the airship |
| 3 | Problem |The only way to reduce the weight of the arm rod aside form changing the material, is to thin out the rod, or to cut out some holes pattern along the length of the road. However, both methods are too risky as it can cause buckling failure along the length of the rod. |
| 4 | Decision |Instead of focusingon the arm rod, the connecter of the arm rod to the airship can also be optimized.|
| 5 | Method |someo fthe exceesive parts of the connector is trimmed off, the thickness is also reduced.|
| 6| Justifications |The weight reduction of the connectors can be accumulate with other structure componet of the arms such as the motor and servo mounts.|
| 7 | Impact |The thruster arm rod and its connector is optimized and redy to be fabricated.|
| 8 | Next Step |Fabricating the thruster arm rod and its connectors.|

<div align="center">

## **Page 7**

</div>

## Log book 07

| No. | Content | Descriptions | 
| ------ | ------ | ------ |
| 1 | Agenda |Fabricating the thruster arm rod and its connectors.|
| 2 | Goals |Finish fabricaring the thruster arm rods and its connectors. |
| 3 | Problem |There are only few 3D printers at the lab and it is occupied by other members of design teams.|
| 4 | Decision |Discuss with the design team to take turns on fabricating components to make the fabricating process smooth and efficient.|
| 5 | Method |Timetable is made that contains what parts that need to be fabricated on which day. Each memebrs that are doing the fabrication also need to update the time needed for their components to finish fabricating.|
| 6| Justifications |The timetable make sure every members aware of the progress of the fabrication.|
| 7 | Impact |Team members can approximate the day the where all the components of the thruster arm is finished fabricated.|
| 8 | Next Step |Assemble the thruster arm and attcth it to the airship's mounts|

<div align="center">

## **Page 8**


